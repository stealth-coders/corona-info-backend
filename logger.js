module.exports = function (winston) {
    var logger = winston.createLogger({
        level: 'info',
        format: winston.format.json(),
        defaultMeta: {
            service: 'user-service'
        },
        transports: [
            //
            // - Write all logs with level `error` and below to `error.log`
            // - Write all logs with level `info` and below to `combined.log`
            //
            //new winston.transports.Console(),
            new winston.transports.File({
                filename: 'error.log',
                level: 'error',
                timestamp: true
            }),
            new winston.transports.File({
                filename: 'combined.log',
                format: winston.format.combine(
                    winston.format.timestamp({
                        format: 'YYYY-MM-DD HH:mm:ss'
                    }),
                    winston.format.json()
                ),
                handleExceptions: true
            })
        ]
    });
    // if (process.env.NODE_ENV !== 'production') {
        logger.add(new winston.transports.Console({
            format: winston.format.simple()
        }));
    // }
    return logger;
}