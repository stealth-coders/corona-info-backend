const {
    Sequelize,
    Model,
    DataTypes
} = require('sequelize');
w = require('winston');
moment = require('moment');
logger = require('./logger')(w);
const TAG = "[Database] ";


// Option 1: Passing a connection URI
const sequelize = new Sequelize("coronainfo", "cinfo", "U4!0RFwT&aL#JaC^", {
    host: "stealth-coders.de",
    port: '3306',
    dialect: 'mysql',
    logging: false,
});


logger.info(TAG + "Connecting to Database...")
sequelize.authenticate().then(() => {
    logger.info(TAG + 'Connection established successfully.');
}).catch(err => {
    logger.error(TAG + 'Unable to connect to the Database:', err);
}).finally(() => {});



const models = {};

const RefElement = sequelize.define("refelement", {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    category: DataTypes.STRING,
    link: DataTypes.STRING,
    type: DataTypes.STRING,
    image: DataTypes.STRING,    
    visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    createdAt: {
        type: DataTypes.DATE,
        //note here this is the guy that you are looking for                   
        get() {
            return moment(this.getDataValue('createdAt')).format('h:mm:ss DD.MM.YYYY');
        }
    },
    updatedAt: {
        type: DataTypes.DATE,
        get() {
            return moment(this.getDataValue('updatedAt')).format('h:mm:ss DD.MM.YYYY');
        }
    }
});

models["refelement"] = RefElement;
const Tag = sequelize.define("tag", {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    color: DataTypes.STRING,
    createdAt: {
        type: DataTypes.DATE,
        //note here this is the guy that you are looking for                   
        get() {
            return moment(this.getDataValue('createdAt')).format('h:mm:ss DD.MM.YYYY');
        }
    },
    updatedAt: {
        type: DataTypes.DATE,
        get() {
            return moment(this.getDataValue('updatedAt')).format('h:mm:ss DD.MM.YYYY');
        }
    }
});
models["tag"] = Tag;


RefElement.belongsToMany(Tag, {
    through: 'refelementHasTags',
    as: 'tags'
});

Tag.belongsToMany(RefElement, {
    through: 'refelementHasTags',
    as: 'tags'
});

// Synchronize all tables with the database
//  Call start
sequelize.sync({
    alter: true
}).then(() => {
    logger.info(TAG + 'Synchronization successful.');
}).catch(err => {
    logger.error(TAG + 'Unable to synchronize the ECenter database:', err);
}).finally(() => {
    // sequelize.close();
});


module.exports.models = models;
module.exports.sequelize = sequelize;