const app = require('express')(),
    request = require('request'),
    w = require('winston'),
    logger = require('./logger')(w),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    database = require('./database'),
    mcache = require('memory-cache');
app.set('view engine', 'jade');

var cache = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url
        let cachedBody = mcache.get(key);
        if (cachedBody) {
            res.send(cachedBody)
            return
        } else {
            res.sendResponse = res.send
            res.send = (body) => {
                mcache.put(key, JSON.parse(body), duration * 1000);
                res.sendResponse(body)
            }
            next()
        }
    }
}

// Global Variables -
const TAG = "[Core] ";
const port = 8082;
const apiVersion = '/v1';


// Express Configuration -
app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}));

// Logging for express -
function logRequest(req, res, next) {
    logger.info(req.url)
    next()
}
app.use(logRequest)

function logError(err, req, res, next) {
    logger.error(err)
    next()
}
app.use(logError)
// - Logging for express

/**
 * Create Ref Element
 */
app.post(apiVersion + "/refelement", (req, res) => {
    let data = req.body;
    let result = null;
    if (data.tags == null || data.tags.length == 0) {
        res.json({
            "error": "no tags provided!"
        });
        return;
    }
    if (data["id"] != null) {
        // Existing display
        result = database.models.refelement.update({
            name: data.name,
            description: data.description,
            link: data.link,
            type: data.type,
            image: data.image,
            category: data.category,
            visible: data.visible
        }, {
            where: {
                id: data.id,
            }
        });
        // Updating tags
        database.models.refelement.findOne({
            where: {
                id: data.id
            }
        }).then((element) => {
            try {
                element.setTags(data.tags);
            } catch (error) {
                logger.error(error);
                res.json({
                    "message": "error",
                    "data": {
                        "msg": "could not set tags"
                    }
                });
                return;
            }
            // Check if the updating was succesful
            result.then(refelement => {
                res.json({
                    "message": "success",
                    "data": refelement
                });
            }).catch(error => {
                logger.error(error);
                res.json({
                    "message": "error",
                    "data": error
                });
            });
        });
    } else {
        let refelement = database.models.refelement.build({
                name: data.name,
                description: data.description,
                link: data.link,
                type: data.type,
                image: data.image,
                category: data.category,
                visible: data.visible
            }).save()
            .then(refelement => {
                // Then create new tag entries
                try {
                    refelement.setTags(data.tags);
                    res.json({
                        "message": "success",
                        "data": refelement
                    });
                } catch (error) {
                    logger.error(error);
                    res.json({
                        "message": "error",
                        "data": {
                            "msg": "could not set tags"
                        }
                    });
                }

            }).catch(error => {
                logger.error(error);
                res.json({
                    "message": "error",
                    "data": error
                });
            });
    }
});

/**
 * Delete a ref element
 */
app.delete(apiVersion + '/refelement', (req, res) => {
    let data = req.body;
    let result = null;
    if (data["id"] != null) {
        // Existing display
        result = database.models.refelement.destroy({
            where: {
                id: data.id,
            }
        });
    }
    result.then(_ => {
            res.json({
                "message": "success",
                "data": {}
            });
        })
        .catch(error => {
            res.json({
                "message": "error",
                "data": error
            });
        });
});


/**
 * Get ref elements
 */
app.get(apiVersion + '/refelement', cache(10 /*TODO increase cache timeout from 10sec to 1hour? */ ), (req, res) => {
    if (req.query.visible == null)
        req.query.visible = true;
    database.models.refelement.findAll({
        attributes: ['id', 'name', 'category', 'description', 'link', 'type', 'image'],
        where: {
            visible: req.query.visible
        },
        include: [{
            association: 'tags',
            attributes: ['id', 'name', 'description', 'color']
        }],
    }).then(data => {
        // Remove all the sub key from dataValues
        for (const element of data) {
            for (const tag of element["tags"]) {
                delete tag.dataValues["refelementHasTags"];
            }
        }
        res.json(data);
    })
});



/**
 * Create Tag Element
 */
app.post(apiVersion + "/tag", (req, res) => {
    let data = req.body;
    let result = null;
    if (data["id"] != null) {
        // Existing display
        result = database.models.tag.update({
            name: data.name,
            description: data.description,
            color: data.color,
        }, {
            where: {
                id: data.id,
            }
        });
    } else {
        let tag = database.models.tag.build({
            name: data.name,
            description: data.description,
            color: data.color,
        });
        // Store to db
        result = tag.save();
    }
    result.then(tag => {
            res.json({
                "message": "success",
                "data": tag
            });
        })
        .catch(error => {
            res.json({
                "message": "error",
                "data": error
            });
        });
});

/**
 * Delete a tag element
 */
app.delete(apiVersion + '/tag', (req, res) => {
    let data = req.body;
    let result = null;
    if (data["id"] != null) {
        // Existing display
        result = database.models.tag.destroy({
            where: {
                id: data.id,
            }
        });
    }
    result.then(_ => {
            res.json({
                "message": "success",
                "data": {}
            });
        })
        .catch(error => {
            res.json({
                "message": "error",
                "data": error
            });
        });
});


/**
 * Get tag elements
 */
app.get(apiVersion + '/tag', cache(10 /*TODO increase cache timeout from 10sec to 1hour? */ ), (req, res) => {
    database.models.tag.findAll({}).then(data => {
        res.json(data);
    })
});





app.listen(port, function () {
    logger.info(TAG + 'CoronaInfo Backend listening on port ' + port + '!');
});